// components/play/paly.js
import '../../service/requert'
import getRequest from '../../service/requert'
const app=getApp()
const audio=app.globalData.audio
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  /**
   * 组件的初始数据
   */
  data: {
   nowTimePre:'',
   duration:0,
   searchData:[],//歌曲id数组
   searchImg:[]//海报id
  },
  ready(){
    let len=app.globalData.searchData.length//搜索歌曲数组长度
    let searchSongmid=[]//歌曲id数组
    let searchImg=[]
    for(let index=0;index<len;index++){
      searchSongmid.push(app.globalData.searchData[index].songmid)
      searchImg.push(app.globalData.searchData[index].albumimg)
    }
    this.setData({
      searchData:searchSongmid,//将数组赋给data属
      searchImg:searchImg
    })
    // console.log(app.globalData.searchData)
    // console.log(app.globalData.nowSongmid)
    // console.log(this.data.searchData)
    let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
    let num
    for(let i in searchSongmid){
      // console.log(i)
      if(searchSongmid[i]==nowSongmid){
        num=i
      }
    }
    console.log(num)
    // console.log(searchSongmid)
    //当歌曲播放完毕后
    audio.onEnded(()=>{
      num=(num + 1)%len
      console.log(num)
      // console.log(searchSongmid)
      let nextsongmid=searchSongmid[num]
      app.globalData.singerImg=searchImg[num]
      // console.log(nextsongmid)
      console.log('eddd..')
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        // console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
        // console.log(res.data.data)
      })
    })
    //当音频监听，实现进度条自动
    audio.onTimeUpdate(()=>{
      audio.currentTime
      audio.duration
      this.setData({
        nowTimePre:audio.currentTime/audio.duration*100
      })
    })
    // console.log(this.data.searchData)
  },
  methods: {
    // 暂停|播放
    isplay:function(){
      if(audio.paused){
        audio.play()
      }else{
        audio.pause()
      }
    },
    //上一首
    preplay:function () {
      audio.stop()
      // console.log('next....')
      let len=this.data.searchData.length;//数组长度
      console.log(len)
      let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
      let searchSongmid=this.data.searchData
      let searchImg=this.data.searchImg
      console.log(nowSongmid)
      let index //=searchSongmid[num]//当前索引
      for(let i in searchSongmid){
       // console.log(i)
        if(searchSongmid[i]==nowSongmid){
          index=i
        }
      }
      console.log('当前索引：'+typeof index)//string
      let num =Number(index)
      num=(num + (len-1) )%len
      console.log('下一个索引:'+num)
      let nextsongmid=searchSongmid[num]//下一首id
      app.globalData.nowSongmid=nextsongmid
      app.globalData.singerImg=searchImg[num]//下一首海报
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        // console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
        app.globalData.lyricData=res.data.data.lyric
       
      })
      console.log(app.globalData.singerImg)
    },
    //下一首
    nextplay:function(){
      audio.stop()
      // console.log('next....')
      let len=this.data.searchData.length;//数组长度
      console.log(len)
      let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
      let searchSongmid=this.data.searchData
      let searchImg=this.data.searchImg
      console.log(nowSongmid)
      let index //=searchSongmid[num]//当前索引
      for(let i in searchSongmid){
       // console.log(i)
        if(searchSongmid[i]==nowSongmid){
          index=i
        }
      }
      console.log('当前索引：'+typeof index)//string
      let num =Number(index)
      num=(num +1 )%len
      console.log('下一个索引:'+num)
      let nextsongmid=searchSongmid[num]//下一首id
      app.globalData.nowSongmid=nextsongmid
      app.globalData.singerImg=searchImg[num]//下一首海报
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        // console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
        app.globalData.lyricData=res.data.data.lyric
       
      })
      console.log(app.globalData.singerImg)
    }
  }

})
