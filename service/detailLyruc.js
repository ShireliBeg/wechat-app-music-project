function detailLyric (lyric) {
  let part = lyric.split('\n')
  for (let index = 0; index < part.length; index++) {
    part[index] = getLyricObj(part[index])
  }
  return part
}
// 处理歌词内部
function getLyricObj (content) {
  let twoParts = content.split(']')
  let time = twoParts[0].substr(1)
  let timeParts = time.split(':')
  let timeSecond = timeParts[1]
  let second = Number(timeSecond)
  let timeMinute = timeParts[0]
  let minute = Number(timeMinute)
  let seconds = minute * 60 + second
  let words = twoParts[1]
  return {
    seconds,
    words
  }
}
export default detailLyric
