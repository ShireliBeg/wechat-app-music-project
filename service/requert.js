function getRequest(url,params) {
    let promise=new Promise((resolve,reject)=>{
        wx.request({
            url:url,
            data:params,
            header:{
                'content-type': 'application/json' // 默认值
            },
            success:(res)=>{
                resolve(res)
            },
            fail:(err)=>{
                reject(err)
            }
        }
        )
    })
    return promise 
}
export default getRequest