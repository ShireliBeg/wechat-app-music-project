# 微信小程序项目微音乐

热门歌曲接口

```js
https://api.zsfmyz.top/music/top

https://api.zsfmyz.top/music/list?p=1&n=30&w=蔡徐坤

https://api.zsfmyz.top/music/song?songmid=000aWBBQ2fMyBJ&guid=126548448

https://api.zsfmyz.top/music/lyric?songmid=000wocYU11tSzS
```

前提：不校验接口



# 底部导航（首页，搜索,热门）

```json
    "list": [
      {
        "pagePath": "pages/index/index",
        "text": "首页",
        "iconPath":"/icons/shouye.png",
        "selectedIconPath":"/icons/shouye.png"
      },
      {
        "pagePath": "pages/music/music",
        "text": "search",
        "iconPath":"/icons/sousuo.png",
        "selectedIconPath":"/icons/sousuo.png"
      },
      {
        "pagePath": "pages/hotTop/hotTop",
        "text": "hotTop",
        "iconPath":"/icons/remen.png",
        "selectedIconPath":"/icons/remen.png"
      }
    ]
```



# 热门音乐（hotTop)

步骤1：获取热门音乐列表,将所需数据保存到data的hotMusicName

```js
  /**
   * 页面的初始数据
   */
  data: {
    hotMusicName:[]
  }
```



```js
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: 'https://api.zsfmyz.top/music/top',
      success:(res)=>{
        this.setData({
          hotMusicName:res.data.data.list
            //便于后面上一首，下一首农夫
          app.globalData.searchData=res.data.data.list
        })
        console.log(res)
      },
      fail:(err)=>{
        console.log(err)
      }
    })
  }
```

步骤2：将数据渲染到页面

```html
<view >热门歌曲</view>
<view wx:for="{{hotMusicName}}" wx:key="id">
    <view 
        class="musicList" 
        bindtap="playMusic" 
        data-songname="{{item.songname}}"
        data-songmid="{{item.songmid}}"> 
    <text  class="musicitem">{{item.songname}}- {{item.singer.name}}</text>  
    </view>
</view>
```

步骤3：点击播放歌曲——获取歌曲id

```js
  playMusic:function (e) { 
    console.log(e.currentTarget.dataset)
   }
```

```js
//代码输出结果
{songmid: "0013KFa32c9lVn", songname: "不爱我"}
```



步骤4：获取音频播放源

封装wx.request

```js
function getRequest(url,params) {
    let promise=new Promise((resolve,reject)=>{
        wx.request({
            url:url,
            data:params,
            header:{
                'content-type': 'application/json' // 默认值
            },
            success:(res)=>{
                resolve(res)
            },
            fail:(err)=>{
                reject(err)
            }
        }
        )
    })
    return promise 
}
export default getRequest
```

将获取的请求数据保存到data中

```js
  playMusic:function (e) { 
    let songmid=e.currentTarget.dataset.songmid
    // console.log(songmid)
    let url='https://api.zsfmyz.top/music/song'
    let params={
      songmid:songmid,
      guid:'126548448'
    }
    console.log(params)
    let promise=getRequest(url,params)
    promise.then(res=>{
      this.setData({
        detailMusic:res.data.data
      })
    })
   }
```

全局配置音乐播放配置（app.json）

```json
  "requiredBackgroundModes": ["audio", "location"]
```

# 播放音源

步骤1：在全局创建wx.createInnerAudioContext()

```js
const myaudio=wx.createInnerAudioContext()
```

步骤2：示例对象赋予音源地址，自动播放

```js
    setTimeout(()=>{
      wx.navigateTo({
        url:'/pages/lyric/lyric'
      })
      audio.src=app.globalData.musicData.musicUrl
      audio.autoplay=true
    },1000)
```



# 搜索音乐

步骤1：基本布局

```html
<view class="searchArea" >
<input 
    class="searchContent" 
    placeholder="请输入电影名称" 
    bindinput="getValue"/>
<text class="searchBtn" bindtap="searchMusic">搜索</text>
</view>
```



 步骤2：输入音乐名点击搜索

```js
  data: {
   musicName:'',
   searchData:[]
  }
```

```js
    // 得到要搜索的歌名
  getValue:function(e){
      this.setData({
        musicName:e.detail.value
      })
      console.log(this.data.musicName)
  }
```

将搜索的数据保存到全局和页面data中

```js
    // 搜索歌曲
  searchMusic:function(){
    let url='https://api.zsfmyz.top/music/list'
    let data={
      p:1,
      n:30,
      w:this.data.musicName
    }
    console.log(data)
    let promise=getRequest(url,data)
    promise.then((res)=>{
      app.globalData.searchData=res.data.data.list
      this.setData({
        searchData:res.data.data.list
      })
      // console.log(res)
    })
    console.log(app.globalData)
  }
```



步骤3：将歌曲渲染至页面

```html
<view wx:for="{{searchData}}" data-songmid="{{item.songmid}}">
<text>{{item.songname}}-{{item.singer.name}}</text>
</view>
```



步骤4：根据id搜索音频音源与歌词

```js
 //点击播放
playSearchMusic:function(e){
    // console.log(e)
    let songmid=e.currentTarget.dataset.songmid
    let params1={
      songmid:songmid,
      guid:'126548448'
    }
    //查找音源
    let promise1=getRequest('https://api.zsfmyz.top/music/song',params1)
    promise1.then((res)=>{
      app.globalData.musicData=res.data.data
      console.log(res)
    })
    let params2={
      songmid:songmid
    }
    //查找歌词
    let promise2=getRequest('https://api.zsfmyz.top/music/lyric',params2)
    promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
      console.log(res)
    })
    //跳转歌词界面
    setTimeout(()=>{
      wx.navigateTo({
        url:'/pages/lyric/lyric'
      })
      audio.src=app.globalData.musicData.musicUrl
      audio.autoplay=true
    },1000)
}
```

# 将歌词,海报渲染到页面上

```html
<image class="blackgornd" src="{{singerImg}}" />
<view class="lyricAtea">
    <view wx:for="{{lyricAll}}" wx:key="index">
        <text>{{item.words}}</text>
    </view>
</view>
```

```js
  data: {
    lyric:'',
    lyricAll:{},
    singerImg:""
  },
   onLoad: function (options) {
    this.setData({
     lyric:app.globalData.lyricData,
     singerImg:app.globalData.singerImg
    })
    let obj=detailLyric(this.data.lyric)//将处理后的歌词保存到变量obj中
    console.log(obj)
    this.setData({
      lyricAll:obj//将歌词保存到页面的data中
    })     
```

# 暂停|播放

```js
    // 暂停|播放
    isplay:function(){
      if(audio.paused){
        audio.play()
      }else{
        audio.pause()
      }
    }
```

# 播放自动条实现

```html
    <progress 
        class="progress" 
        percent="{{nowTimePre}}" 
        color="blue" 
        backgroundColor="white" 
        stroke-width="6" />
```

```js
  data: {
   nowTimePre:'',
   duration:0
  }
```

```js
    audio.onTimeUpdate(()=>{
      audio.currentTime
      audio.duration
      this.setData({
        nowTimePre:audio.currentTime/audio.duration*100
      })
    })
```



# 自动播放下一首

步骤1：将搜索的歌曲全部id组成一个数组

```js
  data: {
   nowTimePre:'',
   duration:0,
   searchData:[],//歌曲id数组
   searchImg:[]//海报id
  }
```



```js
    let len=app.globalData.searchData.length//搜索歌曲数组长度
    let searchSongmid=[]//歌曲id数组
    for(let index=0;index<len;index++){
      searchSongmid.push(app.globalData.searchData[index].songmid)
    }
    this.setData({
      searchData:searchSongmid//将数组赋给data属
    })
```

步骤2：判断当前播放歌曲id所在的索引号,并求下一个索引

```js
    let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
    let num=0
    for(let i in searchSongmid){
      // console.log(i)
      if(searchSongmid[i]==nowSongmid){
        num=i
      }
    }
    //console.log(num)
```

步骤3：监听音源播放结束时

```js
 audio.onEnded(()=>{}
```



步骤4：当音源结束时，得到下一个索引的id，并求音源，歌词

```js
      num=(num + 1)%len//下一首的索引号
      console.log(num)
      // console.log(searchSongmid)
      let nextsongmid=searchSongmid[num]//下一首歌曲id
      app.globalData.singerImg=searchImg[num]//跟新歌手海报
      //console.log(nextsongmid)
      //console.log('eddd..')
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        //console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
        console.log(res.data.data)
      })
```

步骤5：海报，歌词页面渲染更新(lyric.js)

```js
 onLoad: function (options) {
    this.setData({
      lyric:app.globalData.lyricData,
      singerImg:app.globalData.singerImg
     })
     let obj=detailLyric(this.data.lyric)
     // console.log(obj)
     this.setData({
       lyricAll:obj
     })
    //  console.log(this.data.lyricAll)
    //  console.log(this.data.singerImg)
      audio.onEnded(()=>{
        setTimeout(()=>{//延迟1s执行
          this.setData({
            lyric:app.globalData.lyricData,
            singerImg:app.globalData.singerImg
           })
           let obj=detailLyric(this.data.lyric)
           // console.log(obj)
           this.setData({
             lyricAll:obj
           })
           console.log(this.data.lyricAll)
           console.log(this.data.singerImg)
        })
      })
  }
```



完整代码

```js
  ready(){
    let len=app.globalData.searchData.length//搜索歌曲数组长度
    let searchSongmid=[]//歌曲id数组
    for(let index=0;index<len;index++){
      searchSongmid.push(app.globalData.searchData[index].songmid)
    }
    this.setData({
      searchData:searchSongmid//将数组赋给data属
    })
    // console.log(app.globalData.searchData)
    // console.log(app.globalData.nowSongmid)
    // console.log(this.data.searchData)
    let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
    let num=0
    for(let i in searchSongmid){
      // console.log(i)
      if(searchSongmid[i]==nowSongmid){
        num=i
      }
    }
    console.log(num)
    // console.log(searchSongmid)
    //当歌曲播放完毕后
    audio.onEnded(()=>{
      num=(num + 1)%len
      console.log(num)
      // console.log(searchSongmid)
      let nextsongmid=searchSongmid[num]
      app.globalData.singerImg=searchImg[num]//跟新歌手海报
      console.log(nextsongmid)
      console.log('eddd..')
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
        console.log(res.data.data)
      })
    })
    //当音频监听，实现进度条自动
    audio.onTimeUpdate(()=>{
      audio.currentTime
      audio.duration
      this.setData({
        nowTimePre:audio.currentTime/audio.duration*100
      })
    })
    // console.log(this.data.searchData)
  }
```

# 下一首

```js
nextplay:function(){
      audio.stop()
      // console.log('next....')
      let len=this.data.searchData.length;//数组长度
      console.log(len)
      let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
      let searchSongmid=this.data.searchData
      let searchImg=this.data.searchImg
      console.log(nowSongmid)
      let index //当前索引
      for(let i in searchSongmid){
       // console.log(i)
        if(searchSongmid[i]==nowSongmid){
          index=i
        }
      }
      console.log('当前索引：'+typeof index)//string
      let num =Number(index)
      num=(num +1 )%len
      console.log('下一个索引:'+num)
      let nextsongmid=searchSongmid[num]//下一首id
      app.globalData.nowSongmid=nextsongmid
      app.globalData.singerImg=searchImg[num]//下一首海报
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        // console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
        app.globalData.lyricData=res.data.data.lyric
       
      })
      console.log(app.globalData.singerImg)
    }
```

歌词海报更新

```js
    audio.onStop(()=>{
      setTimeout(()=>{
        this.setData({
          lyric:app.globalData.lyricData,
          singerImg:app.globalData.singerImg
         })
         let obj=detailLyric(this.data.lyric)
         // console.log(obj)
         this.setData({
           lyricAll:obj
         })
        //  console.log(this.data.lyricAll)
        //  console.log(this.data.singerImg)
      })
    })
```



# 上一首

```js
 num=(num + (len-1) )%len
```



```js
 preplay:function () {
      audio.stop()
      // console.log('next....')
      let len=this.data.searchData.length;//数组长度
      console.log(len)
      let nowSongmid=app.globalData.nowSongmid//当前播放歌曲id
      let searchSongmid=this.data.searchData
      let searchImg=this.data.searchImg
      console.log(nowSongmid)
      let index //当前索引
      for(let i in searchSongmid){
       // console.log(i)
        if(searchSongmid[i]==nowSongmid){
          index=i
        }
      }
      console.log('当前索引：'+typeof index)//string
      let num =Number(index)
      num=(num + (len-1) )%len
      console.log('下一个索引:'+num)
      let nextsongmid=searchSongmid[num]//下一首id
      app.globalData.nowSongmid=nextsongmid
      app.globalData.singerImg=searchImg[num]//下一首海报
      let url1='https://api.zsfmyz.top/music/song'
      let params1={
        songmid:nextsongmid,
        guid:'126548448'
      }
      // 音源
      let promise1=getRequest(url1,params1)
      promise1.then((res)=>{
        // console.log(res.data.data)
        app.globalData.musicData=res.data.data
        audio.src=res.data.data.musicUrl
      })
      // 歌词
      let url2='https://api.zsfmyz.top/music/lyric'
      let params2={
        songmid:nextsongmid
      }
      let promise2=getRequest(url2,params2)
      promise2.then((res)=>{
        app.globalData.lyricData=res.data.data.lyric
       
      })
      console.log(app.globalData.singerImg)
    }
```



# 模板与组件的区别

# 页面中使用commponent

定义组件play.wxml

```html
<view class="audiopaly">
<button  bindtap="isplay">播放|暂停</button>
<text bindtap="preplay">上一首</text>
<text bindtap="nextplay">下一首</text>
</view>

```

play.js

```js
// components/play/paly.js
Component({
  properties: {
  },
  /**
   * 组件的方法列表
   */
  methods: {
    isplay:function(){
      console.log('paly')
    }
  }
})

```

页面中使用

页面的进行json配置

```json
  "usingComponents": {
    "play":"../../components/play/play"
  }
```

```html
<play></play>
```

页面中使用全局data

```js
  globalData: {
    developAdmin:'YanHilin',
    musicUrl:[]
  }
```



```js
const app = getApp()
console.log(app.globalData)
```



将页面中请求的数据保存到全局data

```js
    const app=getApp()
    promise.then(res=>{
      app.musicUrl=res.data.data
      this.setData({
        detailMusic:res.data.data
      })
    })
```

# 局部页面中使用全局数据

```js
 let app=getApp()


data: {
    lyric:''
  },
  onLoad: function (options) {
    this.setData({
     lyric:app.globalData.lyricData
    })
  }
```

