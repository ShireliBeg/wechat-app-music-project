//app.js
const myaudio=wx.createInnerAudioContext()
App({
  // 监听小程序初始化。
  onLaunch: function () {
  },
  // 监听小程序启动或切前台。
  onShow: function() {
    // console.log('调用onShow......')
  },
  // 监听小程序切后台。
  onHide: function () {
    // console.log('页面隐藏了。。。。')
  },
  // 错误监听函数。
  onError:function () {

  },
  // 页面不存在监听函数。
  onPageNotFound: function () {
    console.log('页面找不到')
  },
  // 未处理的 Promise 拒绝事件监听函数。
  onUnhandledRejection: function () {

  },
  globalData: {
    developAdmin:'YanHilin',
    hotMusic:[],//热歌
    musicData:[],//音源
    audio:myaudio,
    nowSongmid:'',//当前播放歌曲id
    searchData:[],//搜索歌曲列表数组
    lyricData:'',//字符串
    singerImg:''//歌手海报
  }
})