// pages/hotTop/hotTop.js
import '../../service/requert'
import getRequest from '../../service/requert'
const app=getApp()
const audio=app.globalData.audio
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotMusicName:[],
    detailMusic:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: 'https://api.zsfmyz.top/music/top',
      success:(res)=>{
        app.globalData.hotMusic=res.data.data.list
        app.globalData.searchData=res.data.data.list
        this.setData({
          hotMusicName:res.data.data.list
        })
        // console.log(res)
      },
      fail:(err)=>{
        console.log(err)
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  playMusic:function (e) { 
    app.globalData.singerImg=e.currentTarget.dataset.singerimg
    let songmid=e.currentTarget.dataset.songmid
    app.globalData.nowSongmid=songmid
    // console.log(songmid)
    let url='https://api.zsfmyz.top/music/song'
    let params={
      songmid:songmid,
      guid:'126548448'
    }
    // console.log(params)
    let promise=getRequest(url,params)
    promise.then(res=>{
      app.globalData.musicData=res.data.data
    })
    // 获取歌词
    let params2={
      songmid:songmid
    }
    let promise2=getRequest('https://api.zsfmyz.top/music/lyric',params2)
    promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
      // console.log(res)
    })
    // 创建音频播放
    setTimeout(()=>{
      wx.navigateTo({
        url:'/pages/lyric/lyric'
      })
      // audio.src=this.data.detailMusic.musicUrl
      // console.log(app.globalData)
      audio.src=app.globalData.musicData.musicUrl
      audio.autoplay=true
      // console.log(audio)
    },1000)
   }
})