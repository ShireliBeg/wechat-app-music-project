// pages/music/music.js
import '../../service/requert'
import getRequest from '../../service/requert'
const app=getApp()
const audio=app.globalData.audio

Page({
  data: {
   musicName:'',
   searchData:[]
  },
    // 得到要搜索的电影名
  getValue:function(e){
    // console.log(e)
      this.setData({
        musicName:e.detail.value
      })
      // console.log(this.data.musicName)
  },
    // 搜索歌曲
  searchMusic:function(){
    let url='https://api.zsfmyz.top/music/list'
    let data={
      p:1,
      n:30,
      w:this.data.musicName
    }
    // console.log(data)
    let promise=getRequest(url,data)
    promise.then((res)=>{
      app.globalData.searchData=res.data.data.list
      this.setData({
        searchData:res.data.data.list
      })
      // console.log(res)
    })
    // console.log(app.globalData)
  },
  // 
  playSearchMusic:function(e){
    // console.log(e)
    app.globalData.singerImg=e.currentTarget.dataset.singerimg
    // console.log(app.globalData)
    let songmid=e.currentTarget.dataset.songmid
    app.globalData.nowSongmid=songmid
    let params1={
      songmid:songmid,
      guid:'126548448'
    }
    let promise1=getRequest('https://api.zsfmyz.top/music/song',params1)
    promise1.then((res)=>{
      app.globalData.musicData=res.data.data
      // console.log(res)
    })
    let params2={
      songmid:songmid
    }
    let promise2=getRequest('https://api.zsfmyz.top/music/lyric',params2)
    promise2.then((res)=>{
      app.globalData.lyricData=res.data.data.lyric
      // console.log(res)
    })
    setTimeout(()=>{
      wx.navigateTo({
        url:'/pages/lyric/lyric'
      })
      audio.src=app.globalData.musicData.musicUrl
      audio.autoplay=true
    },1000)
  }

})