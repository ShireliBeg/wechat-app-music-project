// pages/lyric.js
import detailLyric from '../../service/detailLyruc'
let app=getApp()
let audio=app.globalData.audio
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lyric:'',
    lyricAll:{},
    singerImg:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      lyric:app.globalData.lyricData,
      singerImg:app.globalData.singerImg
     })
     let obj=detailLyric(this.data.lyric)
     // console.log(obj)
     this.setData({
       lyricAll:obj
     })
    //  console.log(this.data.lyricAll)
    //  console.log(this.data.singerImg)
    audio.onEnded(()=>{
        setTimeout(()=>{
          this.setData({
            lyric:app.globalData.lyricData,
            singerImg:app.globalData.singerImg
           })
           let obj=detailLyric(this.data.lyric)
           // console.log(obj)
           this.setData({
             lyricAll:obj
           })
          //  console.log(this.data.lyricAll)
          //  console.log(this.data.singerImg)
        })
    })
    audio.onStop(()=>{
      setTimeout(()=>{
        this.setData({
          lyric:app.globalData.lyricData,
          singerImg:app.globalData.singerImg
         })
         let obj=detailLyric(this.data.lyric)
         // console.log(obj)
         this.setData({
           lyricAll:obj
         })
        //  console.log(this.data.lyricAll)
        //  console.log(this.data.singerImg)
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})